## The camera is the variant with a 110 angle FOV https://www.adafruit.com/product/4469
import time
import sys
import board
import busio
from os import path, getcwd
from io import open
import adafruit_mlx90640

__location__ = path.realpath(path.join(getcwd(), path.dirname(__file__))) + '/svg/' #some trickery so that the python file can figure out for itself where it is on the harddrive.

i2c = busio.I2C(board.SCL, board.SDA, frequency=800000) #sda and scl refers to the default SDA and SCL on the board detected. On Raspi 4b it's SDA1 and SCL1 (GPIO02 and 03)
mlx = adafruit_mlx90640.MLX90640(i2c) #keep in mind that all the cameras seen so far has identical i2c adresses so that you'd either need to use several i2c channels or somehow change the address
print("MLX addr detected on I2C", [hex(i) for i in mlx.serial_number])

# if using higher refresh rates yields a 'too many retries' exception,
# try decreasing this value to work with certain pi/camera combinations
mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_2_HZ #i.e the times the thermal imaging camera takes a picture every second. In this case, 2 times a second.

frame = [0] * 768 #grid created as a list with 768 elements because the number of pixels of the camera is 24*32=768
#beginning of any svg file. Added to the beginning of every file generated.
svgfilehead = '<svg version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">\n' 

svgfiletail = '</svg>' #End of any svg file. Added to the end of every file generated.
pixelWidth = 50 #how wide should the pixels be
pixelHeight = 50 #how high should the pixels be

def correctColor(temps, tempCount):
    #get highest and lowest temperature
    cMin = tempCount[0]
    cMax = tempCount[len(tempCount)-1]
    len(tempCount)
    colorDistance = 255/(cMax - cMin)


def makeSVG(frame, fname):
    svgFile = open(path.join(__location__, fname),"w") #to make sure that python knows to look for files in the same directory as the .py file
    tempCount = [] #the list containing the unique temperatures, i.e the same temperatures needs to have the same colors.
    temps = [] #the list containing all temperatures.
    pixels = '' #the string containing the pixels formatted as an svg.

    for h in range(24):
        for w in range(32):
            frameVal = round(frame[h*32 + w])
            temps.append(frameVal) #the list containing all the temperatures
            
            x = str(h+pixelWidth) #coordinates for each pixel
            y = str(w+pixelHeight)
            pixels = pixels + '<rect x="' + x + '" y="' + y + '" width="' + str(pixelWidth) + '" height="' + str(pixelHeight) + '" fill="hsl(valuehere, 100%, 50%)"/>\n'
            #we'll add the colors further on in the program.
            #Many of the values are the same. This bit of code adds each unique value to a list tempCount
            if tempCount.count(frameVal) == 0: #if current temperature isn't in the list already. Add it.
                tempCount.append(frameVal)
            else:
                continue
            print('added square')
    tempCount.sort()
    correctColor(temps, tempCount)
    #now we go through pixels and change the values in the HSL to the corrected ones
    pos1 = 1
    pos2 = 77    
    svgFile.write(svgfilehead + pixels + svgfiletail)
    print('pic saved')
    

if __name__ == "__main__":
    while True:
        try:
            fname = str(time.time()) + '.svg' #each file will be saved with the filename: "linux timestamp.svg"
            mlx.getFrame(frame)
            makeSVG(frame, fname)
        except ValueError:
            # these happen, no biggie - retry
            continue
        except KeyboardInterrupt:
            print("Bye!")
            break
    sys.exit()